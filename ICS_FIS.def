###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                     FS - Flow switch with two possible digital inputs                    ##
##                                                                                          ##  
##                                                                                          ##  
############################         Version: 1.3             ################################
# Author:  Miklos Boros
# Date:    03-02-2022
# Version: v1.3
# Changes: 
# 1. Vaiable Name Unification 
############################         Version: 1.0,1.1,1.2             ########################
# Author:  Miklos Boros
# Date:    28-02-2019
# Version: v1.2
# Changes: 
# 1. Major review, 
# 2. Indent,  unit standardization, first release



############################
#  STATUS BLOCK
############################ 
define_status_block()

#Operation modes
add_digital("OpMode_FreeRun",          PV_DESC="Operation Mode FreeRun", PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OpMode_Forced",           PV_DESC="Operation Mode Forced",  PV_ONAM="True",                       PV_ZNAM="False")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",     PV_ONAM="InhibitForce",               PV_ZNAM="AllowForce")

#for OPI visualization
add_digital("EnableFreeRunBtn",        PV_DESC="Enable Free Run Button", PV_ONAM="True",                       PV_ZNAM="False")
add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",    PV_ONAM="True",                       PV_ZNAM="False")

#Transmitter value
add_major_alarm("FlowTooHigh",         "Flow High Alarm",                ARCHIVE=True,                        PV_ZNAM="True")
add_major_alarm("FlowTooLow",          "Flow Low Alarm",                 ARCHIVE=True,                        PV_ZNAM="True")


#Alarm signals
add_major_alarm("GroupAlarm",          "GroupAlarm",                     PV_ZNAM="NominalState")
add_major_alarm("Discrepancy",         "Discrepancy",                    PV_ZNAM="NominalState")
add_major_alarm("HW_ModuleError",      "Module_Error",                   PV_ZNAM="NominalState")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_FreeRun",             PV_DESC="CMD: FreeRun Mode")
add_digital("Cmd_Force",               PV_DESC="CMD: Force Mode")

add_digital("Cmd_FHIGH_ON",            PV_DESC="CMD: Force Value")
add_digital("Cmd_FHIGH_OFF",           PV_DESC="CMD: Force Value")

add_digital("Cmd_FLOW_ON",             PV_DESC="CMD: Force Value")
add_digital("Cmd_FLOW_OFF",            PV_DESC="CMD: Force Value")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()


