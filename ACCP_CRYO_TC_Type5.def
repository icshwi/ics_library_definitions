

################################# Version: 1.0 ##########################################
# ACCP_CRYO_TC_Type4
# Author:  Peyman Sheykholeslami
# Date:    20-03-2024
# Version: v1.0
# Note: Based on existing CosyLab IOC PV records
#########################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()




#Inhibit signals (set by the PLC code, can't be changed by the OPI)


#for OPI visualization

add_analog("Out",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Out",           PV_EGU="%")
add_analog("OutCV34998",    "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="OutCV34998",    PV_EGU="%")
add_analog("OutCV33801",    "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="OutCV33801",    PV_EGU="%")
add_analog("ActSp",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="ActSp",         PV_EGU="K")
add_analog("IntSp",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="IntSp",         PV_EGU="K")

add_digital("CtlOn",        ARCHIVE=" 1Hz",           PV_DESC="CtlOn",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("Ovrrd",        ARCHIVE=" 1Hz",           PV_DESC="Ovrrd",           PV_ONAM="True",           PV_ZNAM="False")

add_enum("StateVis","INT",   PV_NAME="StateVis",  PV_DESC="Status", SCAN="I/O Intr",  PV_NOBT="16", PV_ZRVL="0",      PV_ONVL="1",       PV_TWVL="2",     PV_THVL="3", PV_ZRST="Off",  PV_ONST="Manual On",  PV_TWST="Bypass On",  PV_THST="Control On", PV_ZRSV="MAJOR",  ARCHIVE=False )


#Transmitter value



#Alarm signals




#Feedback
add_analog("BypVal-RB",        "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="BypVal-RB",        PV_EGU="%")
add_analog("OffVal-RB",        "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="OffVal-RB",        PV_EGU="%")
add_analog("SpMax-RB",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpMax-RB",         PV_EGU="K")
add_analog("RmpUp-RB",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpUp-RB",         PV_EGU="K/s")
add_analog("RmpDn-RB",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpDn-RB",         PV_EGU="K/s")
add_analog("Gain-RB",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Gain-RB",          PV_EGU=" ")
add_analog("LowLim-RB",        "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="LowLim-RB",        PV_EGU="%")
add_analog("HiLim-RB",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="HiLim-RB",         PV_EGU="%")
add_analog("DeadB-RB",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="DeadB-RB",         PV_EGU="K")
add_analog("SpMin-RB",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpMin-RB",         PV_EGU="K")
add_analog("SpMinPrs-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpMinPrs-RB",      PV_EGU="bara")
add_analog("SpMaxPrs-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpMaxPrs-RB",      PV_EGU="bara")

add_analog("Tim-RB",           "DWORD",           ARCHIVE=" 1Hz",           PV_DESC="Tim-RB",           PV_EGU="ms")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()


add_analog("BypVal",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="BypVal",         PV_EGU="%")
add_analog("OffVal",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="OffVal",         PV_EGU="%")
add_analog("SpMax",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpMax",          PV_EGU="K")
add_analog("RmpUp",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpUp",          PV_EGU="K/s")
add_analog("RmpDn",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpDn",          PV_EGU="K/s")
add_analog("Gain",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Gain",           PV_EGU=" ")
add_analog("LowLim",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="LowLim",         PV_EGU="%")
add_analog("HiLim",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="HiLim",          PV_EGU="%")
add_analog("DeadB",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="DeadB",          PV_EGU="K")
add_analog("SpMin",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpMin",          PV_EGU="K")
add_analog("SpMinPrs",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpMinPrs",       PV_EGU="bara")
add_analog("SpMaxPrs",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpMaxPrs",       PV_EGU="bara")

add_analog("Tim",           "DINT",           ARCHIVE=" 1Hz",           PV_DESC="Tim",           PV_EGU="ms")







#Forcing

