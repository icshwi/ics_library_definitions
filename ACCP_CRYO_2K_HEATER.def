################################# Version: 1.0 ##########################################
# ACCP_CRYO_2K_HEATER
# Author:  Peyman Sheykholeslami
# Date:    18-07-2024
# Version: v1.0
# Note: Definition file for 2K TEST VESSEL HEATERS CONTROL AND CRYOMODULES HEAT LOAD CALCULATION
#####################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()




#Inhibit signals (set by the PLC code, can't be changed by the OPI)


#for OPI visualization

add_analog("h1",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Helium Enthalpy 4K supply Line",           PV_EGU="J/g")
add_analog("h3",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Helium Enthalpy 2K Test Vessel",           PV_EGU="J/g")
add_analog("h5",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Helium Enthalpy 4K CDS Return",            PV_EGU="J/g")
add_analog("h6",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Helium Enthalpy CDS and 2K Mixed",         PV_EGU="J/g")
add_analog("m2",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="4K CDS Supply mass flow",                  PV_EGU="g/s")
add_analog("m3",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="2K Test Vessel mass flow",                 PV_EGU="g/s")
add_analog("m5",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="4K CDS Return mass flow",                  PV_EGU="g/s")
add_analog("Q1_SP",        "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Apllied Setpoint for Q1",                  PV_EGU="W")
add_analog("Q2_SP",        "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Apllied Setpoint for Q2",                  PV_EGU="W")
add_analog("Q_LINAC",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="LINAC overall heat load",                  PV_EGU="W")
add_analog("Q_CMS",        "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="CMs heat load",                            PV_EGU="W")
add_analog("Q1_Conv",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Q1 Conversation Factor",    	             PV_EGU="W/%",    PV_PINI="YES")
add_analog("Q2_Conv",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Q2 Conversation Factor",    	             PV_EGU="W/%",    PV_PINI="YES")
 
add_analog("Q1_LMN",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Q1 PID Controller Output",    	         PV_EGU="%",      PV_PINI="YES")


add_digital("m6LTmset",             PV_PINI="YES",        ARCHIVE=" 1Hz",   PV_DESC="m6 lower than mset",        PV_ONAM="m6<mset",      PV_ZNAM="m6>=mset")
add_digital("CoolDown",             PV_PINI="YES",        ARCHIVE=" 1Hz",   PV_DESC="CoolDown",       			 PV_ONAM="CoolDown",     PV_ZNAM="CoolDown")
add_digital("Q1_PLCMode_RB",        PV_PINI="YES",        ARCHIVE=" 1Hz",   PV_DESC="Q1 PLC Mode Read Back",	 PV_ONAM="Q1 PLC Mode",  PV_ZNAM="Q1 PLC Mode")
add_digital("Q2_PLCMode_RB",        PV_PINI="YES",        ARCHIVE=" 1Hz",   PV_DESC="Q2 PLC Mode Read Back",	 PV_ONAM="Q2 PLC Mode",  PV_ZNAM="Q2 PLC Mode")


#Transmitter value



#Alarm signals



#Feedback



############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

add_analog("m4",           "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="Coupler cooling Flow",      					PV_EGU="g/s")
add_analog("mset",         "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="CCs set flow from CDS and 2K test vessel",    PV_EGU="g/s")
add_analog("T6",           "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="CDS and 2K TV mixed flow temperature",    	PV_EGU="K")
add_analog("Q_CDS",        "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="The measured CDS Heat Load",    	            PV_EGU="W")
       
       
add_analog("Q1_Min",       "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="Min Value for Manipulated Q1",    	        PV_EGU="%")
add_analog("Q1_Max",       "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="Max Value for Manipulated Q1",    	        PV_EGU="%")
add_analog("Q2_Min",       "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="Min Value for Manipulated Q2",    	        PV_EGU="%")
add_analog("Q2_Max",       "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="Max Value for Manipulated Q2",    	        PV_EGU="%")
   
add_analog("Q1_PID_TI",    "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="Integration time for Q1_PID",    	        PV_EGU="ms")
add_analog("Q1_PID_Gain",  "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="Gain  for Q1_PID",    	                    PV_EGU=" ")
add_analog("Q1_PID_db",    "REAL",     PV_PINI="YES",     PV_PREC="2",    ARCHIVE=" 1Hz",   PV_DESC="Deadband for Q1_PID",    	                PV_EGU="g/s")

add_digital("Q1_PLCMode",        PV_PINI="YES",             ARCHIVE=" 1Hz",     PV_HIGH="2",   PV_DESC="PLC Automatic Mode for Q1")
add_digital("Q2_PLCMode",        PV_PINI="YES",             ARCHIVE=" 1Hz",     PV_HIGH="2",   PV_DESC="PLC Automatic Mode for Q2")

#Forcing

