################################# Version: 1.0 ##########################################
# ACCP_CRYO_BCP_PT
# Author:  Jose Cardoso
# Date:    04-06-2024
# Version: v1.0
# Note:Initial design
#########################################################################################

################################# Version: 2.0 ##########################################
# ACCP_CRYO_BCP_PT
# Author:  Jose Cardoso
# Date:    08-09-2024
# Version: v2.0
# Note:Alarms added
#########################################################################################

################################# Version: 3.0 ##########################################
# ACCP_CRYO_BCP_PT
# Author:  Jose Cardoso
# Date:    16-10-2024
# Version: v3.0
# Note:Dynamic alarm limits for "PV"
#########################################################################################

############################
#  STATUS BLOCK
############################ 
define_status_block()

#for OPI visualization

#Transmitter value
add_analog("Raw",                     "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",             PV_DESC="Raw value")
add_analog("PV",                      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",             PV_DESC="Process value",       PV_EGU="[PLCF#EGU]")
add_major_high_limit("LimHH",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",             PV_DESC="Limit HH",            PV_EGU="[PLCF#EGU]")
add_minor_high_limit("LimH",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",             PV_DESC="Limit H",             PV_EGU="[PLCF#EGU]")
add_major_low_limit("LimLL",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",             PV_DESC="Limit LL",            PV_EGU="[PLCF#EGU]")
add_minor_low_limit("LimL",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",             PV_DESC="Limit L",             PV_EGU="[PLCF#EGU]")

#Transmitter limits
add_analog("ScaleHi",                 "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",             PV_DESC="Scale High",          PV_EGU="[PLCF#EGU]")
add_analog("ScaleLow",                "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",             PV_DESC="Scale low",           PV_EGU="[PLCF#EGU]")

#Transmitter status
add_enum("Sta","BYTE",     PV_NAME="Sta",       PV_DESC="Status",    SCAN="I/O Intr",  PV_NOBT="8",    PV_ZRVL="0",      PV_ONVL="1",       PV_TWVL="2",     PV_THVL="3",     PV_FRVL="4",    PV_FVVL="5",      PV_SXVL="6",     PV_SVVL="7",     PV_ZRST="HighHigh",  PV_ONST="High",  PV_TWST="Low",      PV_THST="LowLow",       PV_FRST="Overrange",       PV_FVST="Underange",         PV_SXST="spare",         PV_SVST="Qbad",   ARCHIVE=True)

#Alarms
add_major_alarm("Underrange",          "Measure Underrange",          PV_DESC="Measure Underrange",        PV_ZNAM="NominalState")    
add_major_alarm("Overrange",           "Measure Underrange",          PV_DESC="Measure Underrange",        PV_ZNAM="NominalState")
add_major_alarm("HIHI",                "Transmitter HIHI",            PV_DESC="Pressure HIHI",             PV_ZNAM="NominalState")
add_minor_alarm("HI",                  "Transmitter HI",              PV_DESC="Pressure HI",               PV_ZNAM="NominalState")
add_minor_alarm("LO",                  "Transmitter LO",              PV_DESC="Pressure LO",               PV_ZNAM="NominalState")
add_major_alarm("LOLO",                "Transmitter LOLO",            PV_DESC="Pressure LOLO",             PV_ZNAM="NominalState")
add_major_alarm("Qbad",                "Transmitter Qbad",            PV_DESC="Transmitter Qbad",          PV_ZNAM="NominalState")