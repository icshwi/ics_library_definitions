###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                             TP - Turbine Pump (with ProfiBus)                            ##
##
############################         Version: 1.3             ################################
# Author:  Miklos Boros
# Date:    10-01-2020
# Version: v1.3
# Changes: 
# 1. Changing according to FAT results 
############################         Version: 1.2             ################################
# Author:  Miklos Boros
# Date:    28-02-2019
# Version: v1.2
# Changes: 
# 1. Major review, 
# 2. Indent,  unit standardization
############################           Version: 1.1           ################################
# Author:  Marino Vojneski 
# Date:    2018-07-09
# Version: v1.1
# Changes: 
# 1. Modified "For OPI visualisation" section: Added digital variable "FB_Keep_Setpoint".
# 2. Modified "Parameter Block" section: Added digital variable "P_Keep_Setpoint".
############################           Version: 1.0           ################################
# Author:  Miklos Boros, Marino Vojneski
# Date:    2018-05-28
# Version: v1.0
##############################################################################################


############################
#  STATUS BLOCK
############################
define_status_block()

#Operation modes
add_digital("OpMode_Auto",             PV_DESC="Operation Mode Auto",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Manual",           PV_DESC="Operation Mode Manual", PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Forced",           PV_DESC="Operation Mode Forced", PV_ONAM="True",           PV_ZNAM="False")

#Valve physical states
add_analog("PumpColor", "INT",         PV_DESC="BlockIcon pump color")

#PB Status Signals
add_digital("Ready",                   PV_DESC="Pump Ready",                              PV_ONAM="True",           PV_ZNAM="False")
add_digital("Running",                 PV_DESC="Pump Running",                            PV_ONAM="True",           PV_ZNAM="False")
add_digital("Stopped",                 PV_DESC="Pump Stopped",                            PV_ONAM="True",           PV_ZNAM="False")
add_digital("RotorMoving",             PV_DESC="Rotor is moving",                         PV_ONAM="True",           PV_ZNAM="False")
add_digital("RestAfterStp",            PV_DESC="Restart only after full stop",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("PLCControlON",            PV_DESC="Profibus Control Enabled",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("SPReached",               PV_DESC="Setpoint Reached",                        PV_ONAM="True",           PV_ZNAM="False")
add_digital("PumpError",               PV_DESC="Pump has error",                          PV_ONAM="True",           PV_ZNAM="False")
add_digital("HWI_Overload",            PV_DESC="Overload",                                PV_ONAM="True",           PV_ZNAM="False")
add_digital("HWI_MaintON",             PV_DESC="Maintenance ON",                          PV_ONAM="True",           PV_ZNAM="False")
add_digital("HeatingON",               PV_DESC="Heating ON",                              PV_ONAM="True",           PV_ZNAM="False")
add_digital("VentValveON",             PV_DESC="Vent Valve ON",                           PV_ONAM="True",           PV_ZNAM="False")
add_digital("LocalOperation",          PV_DESC="LocalOperation",                          PV_ONAM="True",           PV_ZNAM="False")
add_digital("RemoteOperation",         PV_DESC="RemoteOperation",                         PV_ONAM="True",           PV_ZNAM="False")

add_analog("ActPumpSpeed", "INT",      PV_DESC="Actual Pump Speed",             PV_EGU="Hz")
add_analog("MotTemp", "REAL",          PV_DESC="Actual Motor Temperature",      PV_EGU="degC")
add_analog("RotTemp", "REAL",          PV_DESC="Actual Rotor Temperature",      PV_EGU="degC")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",   PV_ONAM="InhibitManual",  PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",    PV_ONAM="InhibitForce",   PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",       PV_ONAM="InhibitLocking", PV_ZNAM="AllowLocking")

#Interlock signals
add_digital("GroupInterlock",          PV_DESC="Group Interlock",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("StartInterlock",          PV_DESC="Start Interlock",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("StopInterlock",           PV_DESC="Stop Interlock",        PV_ONAM="True",           PV_ZNAM="False")

#for OPI visualization
add_digital("EnableAutoBtn",           PV_DESC="Enable Auto Button",    PV_ONAM="True",           PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",  PV_ONAM="True",           PV_ZNAM="False")
add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",   PV_ONAM="True",           PV_ZNAM="False")
add_string("InterlockMsg",             39,                              PV_NAME="InterlockMsg",   PV_DESC="Interlock Message")

#Locking mechanism
add_digital("DevLocked",               PV_DESC="Device Locked",         PV_ONAM="True",           PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                          PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                          PV_DESC="Guest Lock ID")

add_digital("LatchAlarm",                            PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",                            PV_DESC="Group Alarm for OPI")


#Alarm signals
add_major_alarm("Starting_TimeOut",    "Starting Time Out",             PV_ZNAM="NominalState")
add_major_alarm("Stopping_TimeOut",    "Stopping Time Out",             PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error",  "HW Input Module Error",         PV_ZNAM="NominalState")
add_major_alarm("PBModule_Error",      "HW Profibus Module Error",      PV_ZNAM="NominalState")
add_major_alarm("SSTriggered","SSTriggered",                            PV_ZNAM="NominalState")

#OPI timeouts
add_time("StartingTime",               PV_DESC="Starting Time")
add_time("StoppingTime",               PV_DESC="Stopping Time")

############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",               PV_DESC="CMD: Force Mode")
add_digital("Cmd_ManuStart",           PV_DESC="CMD: Manual Start")
add_digital("Cmd_ManuStop",            PV_DESC="CMD: Manual Stop")
add_digital("Cmd_HeatingON",           PV_DESC="CMD: Heating Start")
add_digital("Cmd_HeatingOFF",          PV_DESC="CMD: Heating Stop")
add_digital("Cmd_VentingON",           PV_DESC="CMD: Venting Start")
add_digital("Cmd_VentingOFF",          PV_DESC="CMD: Venting Stop")
add_digital("Cmd_Local",               PV_DESC="CMD: Local Operation")
add_digital("Cmd_Remote",              PV_DESC="CMD: Remote PLC Operation")


add_digital("Cmd_ForceStart",          PV_DESC="CMD: Force Start")
add_digital("Cmd_ForceStop",           PV_DESC="CMD: Force Stop")

add_digital("Cmd_PumpAck",             PV_DESC="CMD: Acknowledge Pump")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_ForceUnlock",         PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",             PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",           PV_DESC="CMD: Unlock Device")

############################
#  PARAMETER BLOCK
############################
define_parameter_block()

#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",                          PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",                          PV_DESC="Device ID after BlockIcon Open")
