###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##      PLC-PLC communication platform for ACCP S7-1500 and S7-400                          ##
##                                                                                          ##  
##                                                                                          ##
############################         Version: 1.0             ################################
# Author:  Peymansheykholeslami
# Date:    11-07-2024
# Version: v1.0
# Changes: 
# 


############################
#  STATUS BLOCK
############################ 
define_status_block()

## Compare Function "Readbacks vs Configurations"
add_analog("Check_sum",   "REAL",  ARCHIVE="TRUE",     PV_PREC="0",      PV_DESC="Compare Check_sum")
add_analog("Counter",     "INT",   ARCHIVE="TRUE",     PV_DESC="Compare Counter")


##Standard communication signals
add_analog("ConnErrorStatus_SND",        "WORD",         ARCHIVE=True,       PV_DESC="Send connection error"    )
add_analog("ConnErrorStatus_RCV",        "WORD",         ARCHIVE=True,       PV_DESC="Receive connection error" )

add_analog("Heartbeat_SND",              "INT",          ARCHIVE=True,       PV_DESC="Send Heartbeat"           )
add_analog("Heartbeat_RCV",              "INT",          ARCHIVE=True,       PV_DESC="Receive Heartbeat"        )



add_digital("SND_Done",         ARCHIVE=" 1Hz",         PV_DESC="Data sent",                     PV_ONAM="True",        PV_ZNAM="False") 
add_digital("SND_Error",        ARCHIVE=" 1Hz",         PV_DESC="Data sent error",               PV_ONAM="True",        PV_ZNAM="False") 
add_digital("SND_Busy",         ARCHIVE=" 1Hz",         PV_DESC="Send operation busy",           PV_ONAM="True",        PV_ZNAM="False") 
add_digital("RCV_Busy",         ARCHIVE=" 1Hz",         PV_DESC="Receive operation busy",        PV_ONAM="True",        PV_ZNAM="False")
add_digital("RCV_Done",         ARCHIVE=" 1Hz",         PV_DESC="Receive operation Done",        PV_ONAM="True",        PV_ZNAM="False")
add_digital("RCV_Error",        ARCHIVE=" 1Hz",         PV_DESC="Receive operation Error",       PV_ONAM="True",        PV_ZNAM="False")
add_digital("ActiveConn",       ARCHIVE=" 1Hz",         PV_DESC="Send connection established",   PV_ONAM="True",        PV_ZNAM="False")
add_digital("ConnError",        ARCHIVE=" 1Hz",         PV_DESC="Send connection error",         PV_ONAM="True",        PV_ZNAM="False") 




#Alarm signals
add_major_alarm("CommError",            "CommError",                                                PV_ZNAM="NominalState")
add_major_alarm("MasterCommError",      "MasterCommError",                                          PV_ZNAM="NominalState")
add_major_alarm("SlaveCommError",       "SlaveCommError",                                           PV_ZNAM="NominalState")


############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons
add_digital("Cmd_Rst_Rcv_com",            ARCHIVE=True,		    PV_DESC="CMD: Reset receiving operation")
add_digital("Cmd_Rst_Snd_com",            ARCHIVE=True,		    PV_DESC="CMD: Reset Sending operation"  )

add_digital("Cmd_En_Snd",                ARCHIVE=True,		    PV_DESC="CMD: Enable Sending operation"  )
add_digital("Cmd_Dis_Snd",               ARCHIVE=True,		    PV_DESC="CMD: Disable Sending operation"  )