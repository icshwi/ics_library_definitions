###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                               Standard State Machine Step                                ##
##                                                                                          ##  
##                                                                                          ##  
############################ Version: 1.0             ########################################
# Author:  Miklos Boros 
# Date:    06-05-2020
# Version: v1.0



############################
#  STATUS BLOCK
############################ 
define_status_block()

#Step Messages
add_string("StrMsg1", 39,         PV_NAME="StrMsg1",            PV_DESC="Step Message 1")
add_string("StrMsg2", 39,         PV_NAME="StrMsg2",            PV_DESC="Step Message 2")


#Status BITs
add_digital("STS_Inactive",  ARCHIVE=" 4Hz",           PV_DESC="Step Inactive",                   PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_Initial",  ARCHIVE=" 4Hz",           PV_DESC="Step Wainting Intital Val.",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_Active",  ARCHIVE=" 4Hz",             PV_DESC="Step Active",                     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_Operator",  ARCHIVE=" 4Hz",           PV_DESC="Step Operator Action",            PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_Finished",  ARCHIVE=" 4Hz",           PV_DESC="Step Finished",                   PV_ONAM="True",                       PV_ZNAM="False")

#Initial condition
add_digital("STS_PosOK",  ARCHIVE=" 4Hz",              PV_DESC="Position OK",                     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_OPMode",  ARCHIVE=" 4Hz",             PV_DESC="Opmode OK",                       PV_ONAM="True",                       PV_ZNAM="False")
add_analog("STS_StepID",              "INT",  ARCHIVE=" 4Hz",                                     PV_DESC="Step Unique ID")
add_analog("STS_ActStep",             "INT",  ARCHIVE=" 4Hz",                                     PV_DESC="Step Actual Step ID")

#Active BlockIcon Buttons
add_digital("BTN_Init_Ena",           PV_DESC="Initialize Button Enable",        PV_ONAM="True",                       PV_ZNAM="False")
add_digital("BTN_Act_Ena",            PV_DESC="Activate Button Enable",          PV_ONAM="True",                       PV_ZNAM="False")

#StartPermissives
add_analog("Permissive1","WORD",      PV_DESC="Start permissives group1")
add_analog("Permissive2","WORD",      PV_DESC="Start permissives group2")
add_analog("Permissive3","WORD",      PV_DESC="Start permissives group3")


#State dependent Interlock
add_minor_alarm("StateInterlock",     "StateInterlock Active",                                    PV_ZNAM="True")

#State alarms
add_digital("GroupAlarm",             PV_DESC="Group Alarm",                     PV_ONAM="True",                       PV_ZNAM="False")
add_minor_alarm("DevInManual",       "DevicesInManual" ,                         PV_DESC="Some Controlled Devices in Manual")
add_major_alarm("Alarm1",            "Alarm1",                                                    PV_ZNAM="True")
add_major_alarm("Alarm2",            "Alarm2",                                                    PV_ZNAM="True")
add_major_alarm("Alarm3",            "Alarm3",                                                    PV_ZNAM="True")
add_major_alarm("Alarm4",            "Alarm4",                                                    PV_ZNAM="True")
add_major_alarm("Alarm5",            "Alarm5",                                                    PV_ZNAM="True")

#Flag bits
add_digital("Flag1",                  PV_DESC="Flag1",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag2",                  PV_DESC="Flag2",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag3",                  PV_DESC="Flag3",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag4",                  PV_DESC="Flag4",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag5",                  PV_DESC="Flag5",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag6",                  PV_DESC="Flag6",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag7",                  PV_DESC="Flag7",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag8",                  PV_DESC="Flag8",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag9",                  PV_DESC="Flag9",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag10",                 PV_DESC="Flag10",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag11",                 PV_DESC="Flag11",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag12",                 PV_DESC="Flag12",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag13",                 PV_DESC="Flag13",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag14",                 PV_DESC="Flag14",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag15",                 PV_DESC="Flag15",     PV_ONAM="True",                       PV_ZNAM="False")

#Parameter Feedback
add_analog("FB_S1",            "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter1")
add_analog("FB_S2",            "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter2")
add_analog("FB_S3",            "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter3")
add_analog("FB_S4",            "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter4")
add_analog("FB_S5",            "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter5")
add_analog("FB_S6",            "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter6")
add_analog("FB_S7",            "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter7")
add_analog("FB_S8",            "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter8")
add_analog("FB_S9",            "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter9")
add_analog("FB_S10",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter10")
add_analog("FB_S11",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter11")
add_analog("FB_S12",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter12")
add_analog("FB_S13",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter13")
add_analog("FB_S14",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter14")
add_analog("FB_S15",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter15")
add_analog("FB_S16",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter16")
add_analog("FB_S17",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter17")
add_analog("FB_S18",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter18")
add_analog("FB_S19",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter19")
add_analog("FB_S20",           "REAL",  PV_PREC="2",                             PV_DESC="Feedback Parameter20")

add_digital("BTN_YES_ENA",            PV_DESC="BTN_YES_ENA",     PV_ONAM="True",                  PV_ZNAM="False")
add_digital("BTN_OK_ENA",             PV_DESC="BTN_OK_ENA",      PV_ONAM="True",                  PV_ZNAM="False")
add_digital("BTN_NO_ENA",             PV_DESC="BTN_NO_ENA",      PV_ONAM="True",                  PV_ZNAM="False")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("CMD_Initialize",         PV_DESC="CMD_Initialize",      PV_ONAM="True",              PV_ZNAM="False")
add_digital("CMD_Activate",           PV_DESC="CMD_Activate",        PV_ONAM="True",              PV_ZNAM="False")

add_digital("CMD_1",                  PV_DESC="CMD_1",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_2",                  PV_DESC="CMD_2",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_3",                  PV_DESC="CMD_3",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_4",                  PV_DESC="CMD_4",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_5",                  PV_DESC="CMD_5",      PV_ONAM="True",                       PV_ZNAM="False")

add_digital("CMD_YES",                PV_DESC="CMD_YES",    PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_OK",                 PV_DESC="CMD_OK",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_NO",                 PV_DESC="CMD_NO",     PV_ONAM="True",                       PV_ZNAM="False")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

# State Machine Operator ON/OFF Bits
add_digital("OP_ONOFF_1",           PV_DESC="Operator ONOFF 1",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_2",           PV_DESC="Operator ONOFF 2",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_3",           PV_DESC="Operator ONOFF 3",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_4",           PV_DESC="Operator ONOFF 4",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_5",           PV_DESC="Operator ONOFF 5",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_6",           PV_DESC="Operator ONOFF 6",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_7",           PV_DESC="Operator ONOFF 7",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_8",           PV_DESC="Operator ONOFF 8",       PV_ONAM="True",                PV_ZNAM="False")

add_analog("S1",            "REAL",  PV_PREC="2",                             PV_DESC="Parameter1")
add_analog("S2",            "REAL",  PV_PREC="2",                             PV_DESC="Parameter2")
add_analog("S3",            "REAL",  PV_PREC="2",                             PV_DESC="Parameter3")
add_analog("S4",            "REAL",  PV_PREC="2",                             PV_DESC="Parameter4")
add_analog("S5",            "REAL",  PV_PREC="2",                             PV_DESC="Parameter5")
add_analog("S6",            "REAL",  PV_PREC="2",                             PV_DESC="Parameter6")
add_analog("S7",            "REAL",  PV_PREC="2",                             PV_DESC="Parameter7")
add_analog("S8",            "REAL",  PV_PREC="2",                             PV_DESC="Parameter8")
add_analog("S9",            "REAL",  PV_PREC="2",                             PV_DESC="Parameter9")
add_analog("S10",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter10")
add_analog("S11",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter11")
add_analog("S12",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter12")
add_analog("S13",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter13")
add_analog("S14",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter14")
add_analog("S15",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter15")
add_analog("S16",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter16")
add_analog("S17",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter17")
add_analog("S18",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter18")
add_analog("S19",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter19")
add_analog("S20",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter20")
add_analog("S21",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter21")
add_analog("S22",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter22")
add_analog("S23",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter23")
add_analog("S24",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter24")
add_analog("S25",           "REAL",  PV_PREC="2",                             PV_DESC="Parameter25")

