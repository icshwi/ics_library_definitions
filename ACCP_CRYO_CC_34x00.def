

################################# Version: 1.0 ##########################################
# ACCP_CRYO_CC_34x00
# Author:  Peyman Sheykholeslami
# Date:    17-05-2024
# Version: v1.0
# Note: Based on existing CosyLab IOC PV records
#########################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()




#Inhibit signals (set by the PLC code, can't be changed by the OPI)


#for OPI visualization

add_analog("StaDlyTim01-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim01-RB",          PV_EGU="s")
add_analog("StaDlyTim02-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim02-RB",          PV_EGU="s")
add_analog("StaDlyTim03-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim03-RB",          PV_EGU="s")
add_analog("StaDlyTim04-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim04-RB",          PV_EGU="s")
add_analog("StaDlyTim05",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim05",             PV_EGU=" ")
add_analog("StaDlyTim06",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim06",             PV_EGU=" ")
add_analog("StaDlyTim07-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim07-RB",          PV_EGU="s")
add_analog("StaDlyTim08-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim08-RB",          PV_EGU="s")
add_analog("StaMaxTim00",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim00",             PV_EGU=" ")
add_analog("StaMaxTim01-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim01-RB",          PV_EGU="s")
add_analog("StaMaxTim02-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim02-RB",          PV_EGU="s")
add_analog("StaMaxTim03-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim03-RB",          PV_EGU="s")
add_analog("StaMaxTim04-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim04-RB",          PV_EGU="m")
add_analog("StaMaxTim05",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim05",             PV_EGU=" ")
add_analog("StaMaxTim06",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim06",             PV_EGU=" ")
add_analog("StaMaxTim07-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim07-RB",          PV_EGU="s")
add_analog("StaMaxTim08-RB",       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim08-RB",          PV_EGU="s")
add_analog("ActStaTimSec",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="ActStaTimSec",            PV_EGU="s")
add_analog("ActStaTimMin",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="ActStaTimMin",            PV_EGU="m")
add_analog("ActStaTimHrs",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="ActStaTimHrs",            PV_EGU="h")
add_analog("ActStaDlySec",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="ActStaDlySec",            PV_EGU="s")
add_analog("ActStaDlyMin",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="ActStaDlyMin",            PV_EGU="m")
add_analog("ActStaDlyHrs",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="ActStaDlyHrs",            PV_EGU="h")
add_analog("SpPrs0-RB",            "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs0-RB",               PV_EGU="bara")
add_analog("SpPrs1-RB",            "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs1-RB",               PV_EGU="bara")
add_analog("SpPrs2-RB",            "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs2-RB",               PV_EGU="bar")
add_analog("SpPrs3-RB",            "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs3-RB",               PV_EGU="bar")
add_analog("SpPrs4-RB",            "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs4-RB",               PV_EGU="bar")
add_analog("SpPrs5-RB",            "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs5-RB",               PV_EGU="bar")
add_analog("DeltaSpdSpP1-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="DeltaSpdSpP1-RB",         PV_EGU="mbar")
add_analog("DeltaSpdSpP2-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="DeltaSpdSpP2-RB",         PV_EGU="mbar")
add_analog("DeltaSpdSpP3-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="DeltaSpdSpP3-RB",         PV_EGU="mbar")
add_analog("RmpSpdSpP0R1-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0R1-RB",         PV_EGU="mbar/s")
add_analog("RmpSpdSpP0R2-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0R2-RB",         PV_EGU="mbar/s")
add_analog("RmpSpdSpP0R3-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0R3-RB",         PV_EGU="mbar/s")
add_analog("MassFlowHold1-RB",     "REAL",           ARCHIVE="1Hz",            PV_PREC="2",          PV_DESC="MassFlowHold1-RB",        PV_EGU="g/s" )
add_analog("MassFlowHold2-RB",     "REAL",           ARCHIVE="1Hz",            PV_PREC="2",          PV_DESC="MassFlowHold2-RB",        PV_EGU="g/s" )
add_analog("MassFlowHold3-RB",     "REAL",           ARCHIVE="1Hz",            PV_PREC="2",          PV_DESC="MassFlowHold3-RB",        PV_EGU="g/s" )
add_analog("GnPrs3StdySt-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="GnPrs3StdySt-RB",         PV_EGU=" ")
add_analog("PmsPrs3EnRun",         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="PmsPrs3EnRun",            PV_EGU="bar")
add_analog("PmsMassFlwEnRun",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="PmsMassFlwEnRun",         PV_EGU="g/s")
add_analog("Tmp0EnRun",            "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Tmp0EnRun",               PV_EGU="K")
add_analog("CurrGnStdySt-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="CurrGnStdySt-RB",         PV_EGU=" ")
add_analog("CurrITStdySt-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="CurrITStdySt-RB",         PV_EGU="s")
add_analog("RmpSpdSpP0Up-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0Up-RB",         PV_EGU="bar/s")
add_analog("RmpSpdSpP0Dn-RB",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0Dn-RB",         PV_EGU="bar/s")
add_analog("ActSelPrsSp",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="ActSelPrsSp",             PV_EGU="bar")
add_analog("ActIntPrsSpStdy",      "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="ActIntPrsSpStdy",         PV_EGU="bara")
add_analog("Prs0Ctl",              "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Prs0Ctl",                 PV_EGU="bar")


add_digital("CmdSeqRst-RB",        ARCHIVE=" 1Hz",           PV_DESC="CmdSeqRst-RB",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("CmdSeqMan-RB",        ARCHIVE=" 1Hz",           PV_DESC="CmdSeqMan-RB",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("CmdSeqAutoOn-RB",     ARCHIVE=" 1Hz",           PV_DESC="CmdSeqAutoOn-RB",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("SigManOn",            ARCHIVE=" 1Hz",           PV_DESC="SigManOn",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("SigAutoOn",           ARCHIVE=" 1Hz",           PV_DESC="SigAutoOn",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("SigOffActv",          ARCHIVE=" 1Hz",           PV_DESC="SigOffActv",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("CV33801IdlPosOk",     ARCHIVE=" 1Hz",           PV_DESC="CV33801IdlPosOk",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaOffBusy",          ARCHIVE=" 1Hz",           PV_DESC="StaOffBusy",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaOFF",              ARCHIVE=" 1Hz",           PV_DESC="StaOFF",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaIdleBusy",         ARCHIVE=" 1Hz",           PV_DESC="StaIdleBusy",              PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaIDLE",             ARCHIVE=" 1Hz",           PV_DESC="StaIDLE",                  PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaStdyStateBsy",     ARCHIVE=" 1Hz",           PV_DESC="StaStdyStateBsy",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaSTEADYSTATE",      ARCHIVE=" 1Hz",           PV_DESC="StaSTEADYSTATE",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaTout00",           ARCHIVE=" 1Hz",           PV_DESC="StaTout00",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaTout01",           ARCHIVE=" 1Hz",           PV_DESC="StaTout01",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaTout02",           ARCHIVE=" 1Hz",           PV_DESC="StaTout02",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaTout03",           ARCHIVE=" 1Hz",           PV_DESC="StaTout03",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaTout04",           ARCHIVE=" 1Hz",           PV_DESC="StaTout04",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaTout05",           ARCHIVE=" 1Hz",           PV_DESC="StaTout05",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaTout06",           ARCHIVE=" 1Hz",           PV_DESC="StaTout06",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaTout07",           ARCHIVE=" 1Hz",           PV_DESC="StaTout07",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("StaTout08",           ARCHIVE=" 1Hz",           PV_DESC="StaTout08",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("SurgeCCS",            ARCHIVE=" 1Hz",           PV_DESC="SurgeCCS",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsGenOk",            ARCHIVE=" 1Hz",           PV_DESC="PmsGenOk",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsWESpLineOpn",      ARCHIVE=" 1Hz",           PV_DESC="PmsWESpLineOpn",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsOutVlvOpn",        ARCHIVE=" 1Hz",           PV_DESC="PmsOutVlvOpn",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsInVlvOpn",         ARCHIVE=" 1Hz",           PV_DESC="PmsInVlvOpn",              PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsInPrsOk",          ARCHIVE=" 1Hz",           PV_DESC="PmsInPrsOk",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsOutPrsOk",         ARCHIVE=" 1Hz",           PV_DESC="PmsOutPrsOk",              PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsWERtnPrsStab",     ARCHIVE=" 1Hz",           PV_DESC="PmsWERtnPrsStab",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsFlwOk",            ARCHIVE=" 1Hz",           PV_DESC="PmsFlwOk",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsTmpOk",            ARCHIVE=" 1Hz",           PV_DESC="PmsTmpOk",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("PmsGrdGasPrsOk",      ARCHIVE=" 1Hz",           PV_DESC="PmsGrdGasPrsOk",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("StTmpSelOn",          ARCHIVE=" 1Hz",           PV_DESC="StTmpSelOn",               PV_ONAM="True",           PV_ZNAM="False")
add_digital("CmdTmpSelOn",         ARCHIVE=" 1Hz",           PV_DESC="CmdTmpSelOn",              PV_ONAM="True",           PV_ZNAM="False")
add_digital("Idling",              ARCHIVE=" 1Hz",           PV_DESC="Idling",                   PV_ONAM="True",           PV_ZNAM="False")

add_enum("Cmd-RB",        "INT",            PV_NAME="Cmd-RB",        PV_DESC="Cmd-RB",     PV_ZRVL="1",    PV_ONVL="2",       PV_TWVL="4",    PV_ZRST="OFF",  PV_ONST="IDLE",  PV_TWST="AUTOMATIC",     ARCHIVE=False)
add_enum("Sta",           "INT",            PV_NAME="Sta",           PV_DESC="Sta",        PV_ZRVL="1",    PV_ONVL="2",       PV_TWVL="4",    PV_ZRST="OFF",  PV_ONST="IDLE",  PV_TWST="AUTOMATIC",     ARCHIVE=False)


#Transmitter value



#Alarm signals

add_major_alarm("SdGen",           "SdGen",                 PV_DESC="SdGen",                 PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdGenHard",       "SdGenHard",             PV_DESC="SdGenHard",             PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdCmnHard",       "SdCmnHard",             PV_DESC="SdCmnHard",             PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdWvcOff",        "SdWvcOff",              PV_DESC="SdWvcOff",              PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdWVCEntrVlvCld", "SdWVCEntrVlvCld",       PV_DESC="SdWVCEntrVlvCld",       PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdSurge",         "SdSurge",               PV_DESC="SdSurge",               PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdGuardGasPrs",   "SdGuardGasPrs",         PV_DESC="SdGuardGasPrs",         PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdCmnSoft",       "SdCmnSoft",             PV_DESC="SdCmnSoft",             PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdInPrsHi",       "SdInPrsHi",             PV_DESC="SdInPrsHi",             PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdOutPrsHi",      "SdOutPrsHi",            PV_DESC="SdOutPrsHi",            PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdTmpHi",         "SdTmpHi",               PV_DESC="SdTmpHi",               PV_ONAM="True",           PV_ZNAM="False")
add_major_alarm("SdCmOff",         "SdCmOff",               PV_DESC="SdCmOff",               PV_ONAM="True",           PV_ZNAM="False")
      


#Feedback




############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

add_analog("StaDlyTim01",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim01",           PV_EGU="s")
add_analog("StaDlyTim02",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim02",           PV_EGU="s")
add_analog("StaDlyTim03",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim03",           PV_EGU="s")
add_analog("StaDlyTim04",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim04",           PV_EGU="s")
add_analog("StaDlyTim07",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim07",           PV_EGU="s")
add_analog("StaDlyTim08",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaDlyTim08",           PV_EGU="s")
add_analog("StaMaxTim01",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim01",           PV_EGU="s")
add_analog("StaMaxTim02",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim02",           PV_EGU="s")
add_analog("StaMaxTim03",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim03",           PV_EGU="s")
add_analog("StaMaxTim04",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim04",           PV_EGU="s")
add_analog("StaMaxTim07",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim07",           PV_EGU="s")
add_analog("StaMaxTim08",           "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="StaMaxTim08",           PV_EGU="s")
add_analog("SpPrs0",                "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs0",                PV_EGU="bara")
add_analog("SpPrs1",                "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs1",                PV_EGU="bara")
add_analog("SpPrs2",                "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs2",                PV_EGU="bar")
add_analog("SpPrs3",                "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs3",                PV_EGU="bar")
add_analog("SpPrs4",                "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs4",                PV_EGU="bar")
add_analog("SpPrs5",                "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="SpPrs5",                PV_EGU="bar")
add_analog("DeltaSpdSpP1",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="DeltaSpdSpP1",          PV_EGU="mbar")
add_analog("DeltaSpdSpP2",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="DeltaSpdSpP2",          PV_EGU="mbar")
add_analog("DeltaSpdSpP3",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="DeltaSpdSpP3",          PV_EGU="mbar")
add_analog("GnPrs3StdySt",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="GnPrs3StdySt",          PV_EGU=" ")
add_analog("CurrGnStdySt",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="CurrGnStdySt",          PV_EGU=" ")
add_analog("CurrITStdySt",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="CurrITStdySt",          PV_EGU=" ")
add_analog("RmpSpdSpP0Up",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0Up",          PV_EGU="bar/s")
add_analog("RmpSpdSpP0Dn",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0Dn",          PV_EGU="bar/s")
add_analog("RmpSpdSpP0R1",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0R1",          PV_EGU="mbar/s")
add_analog("RmpSpdSpP0R2",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0R2",          PV_EGU="mbar/s")
add_analog("RmpSpdSpP0R3",          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="RmpSpdSpP0R3",          PV_EGU="mbar/s")
add_analog("MassFlowHold1",         "REAL",           ARCHIVE="1Hz",            PV_PREC="2",          PV_DESC="MassFlowHold1",         PV_EGU="g/s" )
add_analog("MassFlowHold2",         "REAL",           ARCHIVE="1Hz",            PV_PREC="2",          PV_DESC="MassFlowHold2",         PV_EGU="g/s" )
add_analog("MassFlowHold3",         "REAL",           ARCHIVE="1Hz",            PV_PREC="2",          PV_DESC="MassFlowHold3",         PV_EGU="g/s" )

add_digital("CmdSeqRst",           ARCHIVE=" 1Hz",           PV_DESC="CmdSeqRst",      PV_HIGH="2",      PV_ONAM="True",            PV_ZNAM="False")
add_digital("CmdSeqMan",           ARCHIVE=" 1Hz",           PV_DESC="CmdSeqMan",      PV_HIGH="2",      PV_ONAM="True",            PV_ZNAM="False")
add_digital("CmdSeqAutoOn",        ARCHIVE=" 1Hz",           PV_DESC="CmdSeqAutoOn",   PV_HIGH="2",      PV_ONAM="True",            PV_ZNAM="False")
add_digital("CmdTeSelSw",          ARCHIVE=" 1Hz",           PV_DESC="CmdTeSelSw",     PV_HIGH="2",      PV_ONAM="True",            PV_ZNAM="False")

add_enum("Cmd",     "INT",         PV_NAME="Cmd",            PV_DESC="Cmd",     PV_NOBT="8",       PV_VAL="3",    PV_PINI="YES",      PV_ZRVL="1",      PV_ONVL="2",       PV_TWVL="4",     PV_THVL="0",     PV_ZRST="OFF",  PV_ONST="IDLE",  PV_TWST="AUTOMATIC",  PV_THST="NOACTION",     PV_ASG="LEVEL2",    ARCHIVE=False)


#Forcing

