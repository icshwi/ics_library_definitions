

################################# Version: 1.0 ##########################################
# ACCP_CRYO_AI_Type1
# Author:  Peyman Sheykholeslami
# Date:    20-03-2024
# Version: v1.0
# Note: Based on Device Types on ACCP DB1100
#########################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()

#add_digital("s3",           PV_DESC="Switch 3",      PV_ONAM="True",                    PV_ZNAM="False")
#add_digital("s4",           PV_DESC="Switch 4",      PV_ONAM="True",                    PV_ZNAM="False")
#add_digital("s10",          PV_DESC="Switch 10",     PV_ONAM="True",                    PV_ZNAM="False")
#add_digital("s11",          PV_DESC="Switch 11",     PV_ONAM="True",                    PV_ZNAM="False")
#add_digital("Lo",           PV_DESC="Low",           PV_ONAM="True",                    PV_ZNAM="False")
#add_digital("Hi",           PV_DESC="High",          PV_ONAM="True",                    PV_ZNAM="False")
#add_digital("Act",          PV_DESC="Active",        PV_ONAM="True",                    PV_ZNAM="False")


#Inhibit signals (set by the PLC code, can't be changed by the OPI)
#add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",    PV_ONAM="InhibitManual",           PV_ZNAM="AllowManual")
#add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",     PV_ONAM="InhibitForce",            PV_ZNAM="AllowForce")

#for OPI visualization
#add_digital("EnableFreeRunBtn",        PV_DESC="Enable Free Run Button", PV_ONAM="True",                    PV_ZNAM="False")
#add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",   PV_ONAM="True",                    PV_ZNAM="False")
#add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",    PV_ONAM="True",                    PV_ZNAM="False")
#add_analog("ScaleLOW",                 "REAL",                           PV_DESC="Scale LOW",               PV_EGU="[PLCF#EGU]")
#add_analog("ScaleHIGH",                "REAL",                           PV_DESC="Scale HIGH",              PV_EGU="[PLCF#EGU]")

#Transmitter value
add_analog("Val",                      "REAL",  ARCHIVE=True,                           PV_DESC="Analoge Scaled Value",          PV_EGU="[PLCF#EGU]")
#add_analog("RAWValue",                 "REAL",  ARCHIVE=True,                           PV_DESC="RAW integer scaled" )

#add_digital("LatchAlarm",              PV_DESC="Latching of the alarms")
#add_digital("GroupAlarm",              PV_DESC="Group Alarm for OPI")

#Alarm signals
#add_major_alarm("Underrange",          "Measure Underrange",    PV_DESC="Measure Underrange",     PV_ZNAM="NominalState")
#add_major_alarm("Overrange",           "Measure Overrange",     PV_DESC="Measure Overrange",      PV_ZNAM="NominalState")
add_major_alarm("AHHH",                 "Alarm  HIHIHI",         PV_DESC="Alarm HIHIHI",           PV_ZNAM="NominalState")
add_major_alarm("AHH",                  "Alarm HIHI",            PV_DESC="Alarm HIHI",             PV_ZNAM="NominalState")
add_minor_alarm("AH",                   "Alarm HI",              PV_DESC="Alarm HI",               PV_ZNAM="NominalState")
add_minor_alarm("AL",                   "Alarm LO",              PV_DESC="Alarm LO",               PV_ZNAM="NominalState")
add_major_alarm("ALL",                  "Alarm LOLO",            PV_DESC="Alarm LOLO",             PV_ZNAM="NominalState")
add_major_alarm("ALLL",                 "Alarm LOLOLo",          PV_DESC="Alarm LOLOLO",           PV_ZNAM="NominalState")
add_major_alarm("Err",                  "Error",                 PV_DESC="Error",                  PV_ZNAM="NominalState")
add_major_alarm("Wrn",                  "Warning",               PV_DESC="Warning",                PV_ZNAM="NominalState")
add_major_alarm("Trp",                  "Trip",                  PV_DESC="Trip",                   PV_ZNAM="NominalState")

#Feedback
#add_analog("FB_ForceValue",            "REAL",                           PV_DESC="Feedback Force Pressure", PV_EGU="[PLCF#EGU]")
#add_analog("FB_Limit_HIHI",            "REAL",                           PV_DESC="Feedback Limit HIHI",     PV_EGU="[PLCF#EGU]")
#add_analog("FB_Limit_HI",              "REAL",                           PV_DESC="Feedback Limit HI",       PV_EGU="[PLCF#EGU]")
#add_analog("FB_Limit_LO",              "REAL",                           PV_DESC="Feedback Limit LO",       PV_EGU="[PLCF#EGU]")
#add_analog("FB_Limit_LOLO",            "REAL",                           PV_DESC="Feedback Limit LOLO",     PV_EGU="[PLCF#EGU]")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
#add_digital("Cmd_FreeRun",             ARCHIVE=True,		PV_DESC="CMD: FreeRun Mode")
#add_digital("Cmd_Force",               ARCHIVE=True,		PV_DESC="CMD: Force Mode")
#add_digital("Cmd_ForceVal",            ARCHIVE=True,		PV_DESC="CMD: Force Value")

#add_digital("Cmd_AckAlarm",            ARCHIVE=True,		PV_DESC="CMD: Acknowledge Alarm")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Limits
#add_analog("P_Limit_HIHI",             "REAL",                           PV_DESC="Limit HIHI",              PV_EGU="[PLCF#EGU]")
#add_analog("P_Limit_HI",               "REAL",                           PV_DESC="Limit HI",                PV_EGU="[PLCF#EGU]")
#add_analog("P_Limit_LO",               "REAL",                           PV_DESC="Limit LO",                PV_EGU="[PLCF#EGU]")
#add_analog("P_Limit_LOLO",             "REAL",                           PV_DESC="Limit LOLO",              PV_EGU="[PLCF#EGU]")

#Forcing
#add_analog("P_ForceValue",             "REAL",                           PV_DESC="Force pressure",          PV_EGU="[PLCF#EGU]")
