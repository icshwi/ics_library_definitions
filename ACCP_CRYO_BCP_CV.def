################################# Version: 1.0 ##########################################
# BCP_CV
# Author:  Jose Cardoso
# Date:    05-06-2024
# Version: v1.0
# Note:Initial design
#########################################################################################

################################# Version: 2.0 ##########################################
# BCP_CV
# Author:  Jose Cardoso
# Date:    08-09-2024
# Version: v2.0
# Note:Position transmitter alarms, valve motor alarm
#########################################################################################

################################# Version: 3.0 ##########################################
# BCP_CV
# Author:  Jose Cardoso
# Date:    16-10-2024
# Version: v3.0
# Note:Dynamic alarm limits for "PV"
#########################################################################################

############################
#  STATUS BLOCK
############################ 
define_status_block()

#for OPI visualization


#Valve position transmitter value 
add_analog("Raw",                         "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Raw value")
add_analog("PV",                          "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Process value",       PV_EGU="[PLCF#EGU]")
add_analog("ScaleHi",                     "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Scale High",          PV_EGU="[PLCF#EGU]")
add_analog("ScaleLow",                    "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Scale low",           PV_EGU="[PLCF#EGU]")
add_analog("LimHH",                       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Limit HH",            PV_EGU="bara")
add_analog("LimH",                        "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Limit H",             PV_EGU="bara")
add_analog("LimLL",                       "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Limit LL",            PV_EGU="bara")
add_analog("LimL",                        "REAL",           ARCHIVE=" 1Hz",           PV_PREC="2",          PV_DESC="Limit L",             PV_EGU="bara")


#Alarms
add_major_alarm("Underrange",          "Measure Underrange",         PV_DESC="Measure Underrange",       PV_ZNAM="NominalState")    
add_major_alarm("Overrange",           "Measure Underrange",         PV_DESC="Measure Underrange",       PV_ZNAM="NominalState")
add_major_alarm("HIHI",                "Valve position HIHI",        PV_DESC="Position HIHI",            PV_ZNAM="NominalState")
add_minor_alarm("HI",                  "Valve position HI",          PV_DESC="Position HI",              PV_ZNAM="NominalState")
add_minor_alarm("LO",                  "Valve position LO",          PV_DESC="Position LO",              PV_ZNAM="NominalState")
add_major_alarm("LOLO",                "Valve position LOLO",        PV_DESC="Position LOLO",            PV_ZNAM="NominalState")

#Valve positioner status
add_enum("Sta","BYTE",     PV_NAME="Sta",       PV_DESC="Status",    SCAN="I/O Intr",  PV_NOBT="8",    PV_ZRVL="0",      PV_ONVL="1",       PV_TWVL="2",     PV_THVL="3",     PV_FRVL="4",    PV_FVVL="5",      PV_SXVL="6",     PV_SVVL="7",     PV_ZRST="HighHigh",  PV_ONST="High",  PV_TWST="Low",      PV_THST="LowLow",       PV_FRST="Overrange",       PV_FVST="Underange",         PV_SXST="spare",         PV_SVST="Qbad",   ARCHIVE=True)

#Valve status
add_enum("ValveMotorStatus",    "BYTE",       PV_NAME="ValveMotorStatus",       PV_DESC="Valve Motor Status",    SCAN="I/O Intr",  PV_NOBT="8",    PV_ZRVL="0",      PV_ONVL="1",       PV_TWVL="2",     PV_THVL="3",     PV_FRVL="4",    PV_FVVL="5",      PV_SXVL="6",     PV_SVVL="7",     PV_ZRST="Auto",  PV_ONST="Manual",  PV_TWST="Off",  PV_THST="Running",    PV_FRST="Fault",          PV_FVST="AutoReady",        PV_SXST="Discrepancycontrol",       PV_SVST="Interlock")
add_digital("Auto",                        ARCHIVE=" 1Hz",	             PV_DESC="Valve in auto",              PV_ONAM="True",   PV_ZNAM="False")
add_digital("Manual",                      ARCHIVE=" 1Hz",	             PV_DESC="Valve in manual",            PV_ONAM="True",   PV_ZNAM="False")
add_digital("Closed",                      ARCHIVE=" 1Hz",	             PV_DESC="Valve closed",               PV_ONAM="True",   PV_ZNAM="False")
add_digital("Opened",                      ARCHIVE=" 1Hz",	             PV_DESC="Valve opened",               PV_ONAM="True",   PV_ZNAM="False")
add_digital("AutoReady",                   ARCHIVE=" 1Hz",	             PV_DESC="Valve ready in auto",        PV_ONAM="True",   PV_ZNAM="False")



#Alarms
add_major_alarm("PositionFault",            "PositionFault",               PV_DESC="Valve position fault",          PV_ONAM="Fault")
add_major_alarm("Discrepancycontrol",       "Discrepancycontrol",          PV_DESC="Discrepancy control",           PV_ONAM="Fault")




