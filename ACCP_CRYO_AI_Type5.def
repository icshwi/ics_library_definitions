

################################# Version: 1.0 ##########################################
# ACCP_CRYO_AI_Type5
# Author:  Peyman Sheykholeslami
# Date:    27-06-2024
# Version: v1.0
# Note: Based on Device Types on ACCP DB1100
#########################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()




#Inhibit signals (set by the PLC code, can't be changed by the OPI)


#for OPI visualization


#Transmitter value

add_analog("Val",      "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="Analoge Scaled Value",      PV_EGU="[PLCF#EGU]")
add_analog("ValCalc",  "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="Calculated Value",          PV_EGU="[PLCF#EGU]")


#OPI Visualization

add_analog("LimStabHyst-RB",      "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="LimStabHyst-RB",     PV_EGU="bara")
add_analog("LimCondWrn-RB",       "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="LimCondWrn-RB",      PV_EGU="bara")
add_analog("LimCondAl-RB",        "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="LimCondAl-RB",       PV_EGU="bara")
add_analog("MinPrs-RB",           "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="MinPrs-RB",          PV_EGU="bara")
add_analog("SwOnTmp80K-RB",       "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="SwOnTmp80K-RB",      PV_EGU="K")
add_analog("OffTOffs80K-RB",      "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="OffTOffs80K-RB",     PV_EGU="K")
add_analog("SwOnTmp20K-RB",       "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="SwOnTmp20K-RB",      PV_EGU="K")
add_analog("OffTOffs20K-RB",      "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="OffTOffs20K-RB",     PV_EGU="K")
add_analog("SwOnTmp6K-RB",        "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="SwOnTmp6K-RB",       PV_EGU="K")
add_analog("SwOffTOffs6K-RB",     "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="SwOffTOffs6K-RB",    PV_EGU="K")
add_analog("AHHDly-RB",           "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="AHHDly-RB",          PV_EGU=" ")
add_analog("ALLDly-RB",           "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="ALLDly-RB",          PV_EGU=" ")
add_analog("dPrOpn-RB",           "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="dPrOpn-RB",          PV_EGU="bar")
add_analog("LowLim-RB",           "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="LowLim-RB",          PV_EGU="bara")
add_analog("HiLim-RB",            "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="HiLim-RB",           PV_EGU="bara")
add_analog("dTmpMin-RB",          "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="dTmpMin-RB",         PV_EGU="K")
add_analog("dTmpMax-RB",          "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="dTmpMax-RB",         PV_EGU="K")
add_analog("LimVal-RB",           "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="LimVal-RB",          PV_EGU="K")
add_analog("NoLvl-RB",            "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="NoLvl-RB",           PV_EGU="%")
add_analog("dPrsMax-RB",          "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="dPrsMax-RB",         PV_EGU="bar")
add_analog("DwrRtnCold-RB",       "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="DwrRtnCold-RB",      PV_EGU="K")
add_analog("MinTmpOffs-RB",       "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="MinTmpOffs-RB",      PV_EGU="K")
add_analog("MinPrOffs-RB",        "REAL",       ARCHIVE=" 1Hz",     PV_PREC="2",    MDEL="0.1",       ADEL="0.1",         PV_DESC="MinPrOffs-RB",       PV_EGU="bar")


add_digital("MinVcmOk",           ARCHIVE=" 1Hz",           PV_DESC="MinVcmOk",           PV_ONAM="Ok",           PV_ZNAM="Not Ok")
add_digital("VcmPrsOk",           ARCHIVE=" 1Hz",           PV_DESC="VcmPrsOk",           PV_ONAM="Ok",           PV_ZNAM="Not Ok")

#Alarm signals


add_major_alarm("AHHH",               "Alarm  HIHIHI",         PV_DESC="Alarm HIHIHI",           PV_ZNAM="NominalState")
add_major_alarm("AHH",                "Alarm HIHI",            PV_DESC="Alarm HIHI",             PV_ZNAM="NominalState")
add_minor_alarm("AH",                 "Alarm HI",              PV_DESC="Alarm HI",               PV_ZNAM="NominalState")
add_minor_alarm("AL",                 "Alarm LO",              PV_DESC="Alarm LO",               PV_ZNAM="NominalState")
add_major_alarm("ALL",                "Alarm LOLO",            PV_DESC="Alarm LOLO",             PV_ZNAM="NominalState")
add_major_alarm("ALLL",               "Alarm LOLOLo",          PV_DESC="Alarm LOLOLO",           PV_ZNAM="NominalState")
add_major_alarm("Err",                "Error",                 PV_DESC="Error",                  PV_ZNAM="NominalState")
add_major_alarm("Wrn",                "Warning",               PV_DESC="Warning",                PV_ZNAM="NominalState")
add_major_alarm("Trp",                "Trip",                  PV_DESC="Trip",                   PV_ZNAM="NominalState")
add_major_alarm("ClcHL",              "ClcHL",                 PV_DESC="ClcHL",          		   PV_ZNAM="NominalState")
add_major_alarm("ClcLL",              "ClcLL",                 PV_DESC="ClcLL",                  PV_ZNAM="NominalState")
add_major_alarm("SigFlt",             "SigFlt",                PV_DESC="SigFlt",          		   PV_ZNAM="NominalState")
add_major_alarm("PosFlt",             "PosFlt",                PV_DESC="PosFlt",          		   PV_ZNAM="NominalState")

#Feedback



############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Limits

add_analog("LimStabHyst",     "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="LimStabHyst",    PV_EGU="bara")
add_analog("LimCondWrn",      "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="LimCondWrn",     PV_EGU="bara")
add_analog("LimCondAl",       "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="LimCondAl",      PV_EGU="bara")  
add_analog("MinPrs",          "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="MinPrs",         PV_EGU="bara")    
add_analog("SwOnTmp80K",      "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="SwOnTmp80K",     PV_EGU="K")
add_analog("OffTOffs80K",     "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="OffTOffs80K",    PV_EGU="K")
add_analog("SwOnTmp20K",      "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="SwOnTmp20K",     PV_EGU="K")
add_analog("OffTOffs20K",     "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="OffTOffs20K",    PV_EGU="K")
add_analog("SwOnTmp6K",       "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="SwOnTmp6K",      PV_EGU="K")
add_analog("SwOffTOffs6K",    "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="SwOffTOffs6K",   PV_EGU="K")
add_analog("AHHDly",          "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="AHHDly",         PV_EGU=" ")
add_analog("ALLDly",          "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="ALLDly",         PV_EGU=" ")
add_analog("dPrOpn",          "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="dPrOpn",         PV_EGU="bar")
add_analog("LowLim",          "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="LowLim",         PV_EGU="bara")
add_analog("HiLim",           "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="HiLim",          PV_EGU="bara")
add_analog("dTmpMin",         "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="dTmpMin",        PV_EGU="K")
add_analog("dTmpMax",         "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="dTmpMax",        PV_EGU="K")
add_analog("LimVal",          "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="LimVal",         PV_EGU="K")
add_analog("NoLvl",           "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="NoLvl",          PV_EGU="%")
add_analog("dPrsMax",         "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="dPrsMax",        PV_EGU="bar")
add_analog("DwrRtnCold",      "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="DwrRtnCold",     PV_EGU="K")
add_analog("MinTmpOffs",      "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="MinTmpOffs",     PV_EGU="K")
add_analog("MinPrOffs",       "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",    PV_DESC="MinPrOffs",      PV_EGU="bar")



#Forcing

