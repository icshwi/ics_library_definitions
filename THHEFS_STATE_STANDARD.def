###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     #################################### 
##                                                                                          ##  
##                               Standard State Machine State for THHeFS                    ##
##                                                                                          ##  
##                                                                                          ##
############################         Version: 1.1             ################################
# Author:  Attila Horvath
# Date:    23-01-2024
# Version: v1.0
# Changes:      
##############################################################################################

############################
#  STATUS BLOCK
############################ 
define_status_block()

#Step Messages
add_string("StrMsg1", 39,         PV_NAME="StrMsg1",            	PV_DESC="Step Message 1")
add_string("StrMsg2", 39,         PV_NAME="StrMsg2",            	PV_DESC="Step Message 2")


#Status BITs
add_digital("STS_Active",       	ARCHIVE=True,           		PV_DESC="FSM Machine Active",           	PV_ONAM="True",		PV_ZNAM="False")

#Initial condition
add_digital("STS_RequisitsOK",     	ARCHIVE=True,             		PV_DESC="State can be activated",           PV_ONAM="True",		PV_ZNAM="False")
add_analog("STS_StateID",	"INT",                   				PV_DESC="Step Unique ID")   
add_digital("STS_Operator",     	ARCHIVE=True,             		PV_DESC="Operator intervention",            PV_ONAM="True",		PV_ZNAM="False")

#Active BlockIcon Buttons
add_digital("ENA_Activate",           								PV_DESC="Enable Activate Button",           PV_ONAM="True",		PV_ZNAM="False")
add_digital("Vis_Activate",           								PV_DESC="Visible Activate Button",          PV_ONAM="True",		PV_ZNAM="False")

#State alarms -> EPICS major Alarms
add_digital("GroupAlarm",             								PV_DESC="Group Alarm",                     	PV_ONAM="True",     PV_ZNAM="False")
add_major_alarm("Alarm1",            "Alarm1",                                                    									PV_ZNAM="True")
add_major_alarm("Alarm2",            "Alarm2",                                                    									PV_ZNAM="True")
add_major_alarm("Alarm3",            "Alarm3",                                                    									PV_ZNAM="True")
add_major_alarm("Alarm4",            "Alarm4",                                                    									PV_ZNAM="True")
add_major_alarm("Alarm5",            "Alarm5",                                                    									PV_ZNAM="True")

#State warnings -> EPICS minor Alarms
add_digital("GroupWarning",                                                 	PV_DESC="Monitoring Function  Warning",                PV_ONAM="True",                       PV_ZNAM="False")
add_minor_alarm("Warning1",          "Warning1",                                                  PV_ZNAM="True")
add_minor_alarm("Warning2",          "Warning2",                                                  PV_ZNAM="True")
add_minor_alarm("Warning3",          "Warning3",                                                  PV_ZNAM="True")
add_minor_alarm("Warning4",          "Warning4",                                                  PV_ZNAM="True")
add_minor_alarm("Warning5",          "Warning5",                                                  PV_ZNAM="True")

#State warnings -> Only notification on the OPI
add_digital("OPI_Warning1",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning2",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning3",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning4",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning5",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning6",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning7",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning8",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning9",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning10",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")


add_digital("BTN_YES_ENA",            								PV_DESC="BTN_YES_ENA",     PV_ONAM="True",                  PV_ZNAM="False")
add_digital("BTN_OK_ENA",             								PV_DESC="BTN_OK_ENA",      PV_ONAM="True",                  PV_ZNAM="False")
add_digital("BTN_NO_ENA",             								PV_DESC="BTN_NO_ENA",      PV_ONAM="True",                  PV_ZNAM="False")


############################
#  COMMAND BLOCK
############################ 
define_command_block()
add_digital("CMD_Activate",			PV_DESC="BTN Activate Button",                         	PV_ONAM="True",                       PV_ZNAM="False")

add_digital("CMD_YES",				PV_DESC="CMD_YES",    									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_OK",				PV_DESC="CMD_OK",     									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_NO",				PV_DESC="CMD_NO",     									PV_ONAM="True",                       PV_ZNAM="False")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

