
################################# Version: 1.0 ##########################################
# ACCP_CRYO_T_Type3
# Author:  Jose Cardoso
# Date:    07-05-2024
# Version: v1.0
# Note: Based on CosyLab IOC for ACCP
#####################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()


add_digital("MsgRun",  	    ARCHIVE=" 1Hz",     PV_DESC="T_MsgRun",          PV_ONAM="Message",       PV_ZNAM="No Message")
add_digital("Turn",             ARCHIVE=" 1Hz",     PV_DESC="T_Turn",            PV_ONAM="Active",        PV_ZNAM="No Active")
add_digital("Cond",             ARCHIVE=" 1Hz",     PV_DESC="T_Cond",            PV_ONAM="Active",        PV_ZNAM="No Active")
add_digital("BrngGas",          ARCHIVE=" 1Hz",     PV_DESC="T_BrngGas",         PV_ONAM="Active",        PV_ZNAM="No Active")
add_digital("Purge",            ARCHIVE=" 1Hz",     PV_DESC="T_Purge",           PV_ONAM="Active",        PV_ZNAM="No Active")
add_digital("BrngGasOn",        ARCHIVE=" 1Hz",     PV_DESC="T_BrngGasOn",       PV_ONAM="Active",        PV_ZNAM="No Active")
add_digital("NotTurn",          ARCHIVE=" 1Hz",     PV_DESC="T_NotTurn",         PV_ONAM="Active",        PV_ZNAM="No Active")
add_digital("CwFlwOk",          ARCHIVE=" 1Hz",     PV_DESC="T_CwFlwOk",         PV_ONAM="Active",        PV_ZNAM="No Active")
add_digital("Warning",          ARCHIVE=" 1Hz",     PV_DESC="T_Warning",         PV_ONAM="Active",        PV_ZNAM="No Active")
add_digital("OverOffSpeed",     ARCHIVE=" 1Hz",     PV_DESC="T_OverOffSpeed",    PV_ONAM="Active",        PV_ZNAM="No Active")
add_digital("TotReset-RB",      ARCHIVE=" 1Hz",     PV_DESC="T_TotReset-RB",     PV_ONAM="Reset",         PV_ZNAM="No Reset")
add_digital("TotSet-RB",        ARCHIVE=" 1Hz",     PV_DESC="T_TotSet-RB",       PV_ONAM="Set",           PV_ZNAM="No Set")
add_digital("CondEnd",          ARCHIVE=" 1Hz",     PV_DESC="T_CondEnd",         PV_ONAM="Active",        PV_ZNAM="No Active")


add_analog("TotTimDays",       "DINT",     ARCHIVE=" 1Hz",       PV_DESC="T_TotTimDays",      PV_EGU="d")
add_analog("TotTimHrs",        "INT",     ARCHIVE=" 1Hz",        PV_DESC="T_TotTimHrs",       PV_EGU="h")
add_analog("TotTimMin",        "INT",     ARCHIVE=" 1Hz",        PV_DESC="T_TotTimMin",       PV_EGU="min")
add_analog("TotTimSec",        "INT",     ARCHIVE=" 1Hz",        PV_DESC="T_TotTimSec",       PV_EGU="s")
add_analog("SumTotTmHrs",      "DINT",     ARCHIVE=" 1Hz",       PV_DESC="T_SumTotTmHrs",     PV_EGU="h")
add_analog("TotSetTimDays",    "DINT",     ARCHIVE=" 1Hz",       PV_DESC="T_TotSetTimDays",   PV_EGU="d")
add_analog("TotSetTimHrs",     "INT",     ARCHIVE=" 1Hz",        PV_DESC="T_TotSetTimHrs",    PV_EGU="h")
add_analog("TotSetTimMin",     "INT",     ARCHIVE=" 1Hz",        PV_DESC="T_TotSetTimMin",    PV_EGU="min")
add_analog("TotSetTimSec",     "INT",     ARCHIVE=" 1Hz",        PV_DESC="T_TotSetTimSec",    PV_EGU="s")
add_analog("CalcOutPrs",       "REAL",     ARCHIVE=" 1Hz",       PV_PREC="2",      PV_DESC="T_CalcOutPrs",        PV_EGU="bar")

add_analog("SumTotTimHrs",     "DINT",     ARCHIVE=" 1Hz",             PV_DESC="SumTotTimHrs",    PV_EGU="h")

#Alarms

add_major_alarm("SdPrsRatio",         "SdPrsRatio_isAlrm",          PV_DESC="T_SdPrsRatio",          PV_ONAM="Shutdown",       PV_ZNAM="No Shutdown")
add_major_alarm("SdBrngGas",          "SdBrngGas_isAlrm",           PV_DESC="S_SdBrngGas",           PV_ONAM="Trip",           PV_ZNAM="No Trip")



############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

add_digital("TotReset",           PV_PINI="YES",       ARCHIVE=" 1Hz",         PV_DESC="T_TotReset",      PV_ONAM="Reset",     PV_ZNAM="No Reset",         PV_ASG="LEVEL2")
add_digital("TotSet",             PV_PINI="YES",       ARCHIVE=" 1Hz",         PV_DESC="T_TotSet",        PV_ONAM="Set",       PV_ZNAM="No Set",           PV_ASG="LEVEL2")


add_analog("TotSetTmDays",    "DINT",     PV_PINI="YES",     ARCHIVE=" 1Hz",   PV_DESC="T_TotSetTmDays",           PV_EGU="d",       PV_ASG="LEVEL2")
add_analog("TotSetTmHours",   "DINT",     PV_PINI="YES",     ARCHIVE=" 1Hz",   PV_DESC="T_TotSetTmHours",          PV_EGU="h",       PV_ASG="LEVEL2")
add_analog("TotSetTmMin",     "DINT",     PV_PINI="YES",     ARCHIVE=" 1Hz",   PV_DESC="T_TotSetTmMin",            PV_EGU="min",     PV_ASG="LEVEL2")
add_analog("TotSetTmSec",     "DINT",     PV_PINI="YES",     ARCHIVE=" 1Hz",   PV_DESC="T_TotSetTmSec",            PV_EGU="s",       PV_ASG="LEVEL2")

