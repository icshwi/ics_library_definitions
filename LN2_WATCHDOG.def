###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ##
##  CCDB device types: ICS_xxxxx                                                            ##
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##
##                          LT- Level Transmitter in percent                                ##
##                                                                                          ##
##                                                                                          ##
############################         Version: 1.0            ################################
#
# Author:  Dominik Domagala
# Date:    23-05-2024
# Version: v1.0


############################
#  STATUS BLOCK
############################
define_status_block()

#Operation modes
add_digital("OpMode_Auto",    ARCHIVE=" 1Hz",                       PV_DESC="Operation Mode Auto",        PV_ONAM="True",            PV_ZNAM="False")      
add_digital("OpMode_Manual",  ARCHIVE=" 1Hz",                       PV_DESC="Operation Mode Manual",      PV_ONAM="True",             PV_ZNAM="False")

#Status signals
add_digital("Decreasing",   ARCHIVE=" 1Hz",       PV_DESC="Level decreasing", PV_ONAM="True",               PV_ZNAM="False")
add_digital("Increasing",    ARCHIVE=" 1Hz",       PV_DESC="Level increasing",  PV_ONAM="True",             PV_ZNAM="False")
add_digital("Stable",       ARCHIVE=" 1Hz",         PV_DESC="Level stable",  PV_ONAM="True",                PV_ZNAM="False")
add_digital("EnableFreeRunBtn",        PV_DESC="Enable Free Run Button", PV_ONAM="True",                    PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",   PV_ONAM="True",                    PV_ZNAM="False")


#Alarm signals
add_major_alarm("LevelDecreasing", "Level Decreasing")
add_minor_alarm("LevelIncreasing", "Level Increasing")

#Feedback
add_string("DecrTimeStmp-RB",39,           ARCHIVE=" 1Hz",       PV_NAME="DecrTimeStmp-RB",       PV_DESC="Time stamp when level going down")
add_string("DecrTime-RB",39,               ARCHIVE=" 1Hz",       PV_NAME="DecrTime-RB",            PV_DESC="Time when lvl star decr and stop")
add_string("StabTimeStmp-RB",39,           ARCHIVE=" 1Hz",      PV_NAME="StabTimeStmp-RB",       PV_DESC="Time stamp when level in stabilize")
add_string("IncrTimeStmp-RB",39,           ARCHIVE=" 1Hz",      PV_NAME="IncrTimeStamp-RB",      PV_DESC="Time stamp when level Increasing")
add_string("IncrTime-RB",39,               ARCHIVE=" 1Hz",      PV_NAME="IncrTime-RB",           PV_DESC="Time  when level incr and stop")
add_string("FillingTime-RB",39,            ARCHIVE=" 1Hz",     PV_NAME="FillingTime-RB",           PV_DESC="Filling time")
add_analog("DrainTime-RB",        "INT",              ARCHIVE=" 1Hz",    PV_EGU="day/s",           PV_DESC="Draining time")
add_analog("Sample1-RB",         "REAL",         ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="%",      PV_DESC="sample 1")
add_analog("Sample2-RB",         "REAL",         ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="%",      PV_DESC="sample 2")
add_analog("Sample3-RB",         "REAL",         ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="%",     PV_DESC="sample 3")
add_analog("Sample4-RB",         "REAL",         ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="%",      PV_DESC="sample 4")
add_analog("Sample5-RB",         "REAL",         ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="%",      PV_DESC="sample 5")
add_analog("Sample6-RB",         "REAL",   ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="%",      PV_DESC="sample 6")
add_analog("SampleAvg-RB",       "REAL",   ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="%",      PV_DESC="sample average")
add_analog("FlowDraining-RB",    "REAL",   ARCHIVE=" 14Hz",     PV_PREC="2",PV_EGU="m3/h",  PV_DESC="flow during decreasing")
add_analog("FlowFilling-RB",     "REAL",   ARCHIVE=" 14Hz",     PV_PREC="2",PV_EGU="m3/h",     PV_DESC="flow during filing")
add_analog("DecrCompLimit-RB",   "REAL",   ARCHIVE=" 1Hz",     PV_PREC="2",      PV_DESC="Readback Decreasion limit for compare")
add_analog("IncrCompLimit-RB",   "REAL",   ARCHIVE=" 1Hz",     PV_PREC="2",      PV_DESC="Readback Increasion limit for compare")
add_analog("Deadband-RB",        "REAL",   ARCHIVE=" 1Hz",     PV_PREC="4",      PV_DESC="Readback Deadband wide for stable")
add_analog("SamplingTime-RB",    "REAL",   ARCHIVE=" 1Hz",     PV_PREC="2",      PV_DESC="Readback Sampling time")
add_analog("DiffDecrLevel",      "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",  PV_DESC="Readback of amount decreased")
add_analog("DiffIncrLevel",      "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="%",    PV_DESC="Readback of amount Increased")
add_analog("DiffDecrVolume",      "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",  PV_DESC="Readback of amount decreased")
add_analog("DiffIncrVolume",      "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",    PV_DESC="Readback of amount Increased")
add_analog("DailyUsage-RB",    "REAL",   ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="m3",  PV_DESC="daily usage to archive")
add_analog("WeeklyUsage-RB",   "REAL",     ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="weekly usage")
add_analog("MonthlyUsage-RB",   "REAL",     ARCHIVE=" 1Hz",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="monthly usage")


#arrays for storing values

define_plc_array("DayUsage")
add_analog("DayUsage1",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Monday usage")
add_analog("DayUsage2",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Tuesday Usage")
add_analog("DayUsage3",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Wednesday usage")
add_analog("DayUsage4",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Thursday Usage")
add_analog("DayUsage5",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Friday usage")
add_analog("DayUsage6",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Saturday Usage")
add_analog("DayUsage7",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Sunday Usage")
end_plc_array()

define_plc_array("WeekUsage")
add_analog("WeekUsage1",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Week1 usage")
add_analog("WeekUsage2",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Week2 Usage")
add_analog("WeekUsage3",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Week3 usage")
add_analog("WeekUsage4",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="Week4 Usage")
end_plc_array()

add_analog("MonthlyUsage",   "REAL",     ARCHIVE=" YES",     PV_PREC="2",PV_EGU="m3",                     PV_DESC="monthly usage")

#readings and feedbacks

add_analog("Level-RB",           "REAL",   ARCHIVE=" YES",     PV_PREC="2", PV_EGU="%" ,     PV_DESC="Readback of level from LT")
add_analog("Volume-R",           "REAL",   ARCHIVE=" YES",     PV_PREC="2", PV_EGU="m3",    PV_DESC="calculated value of nitrogen")

############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons

add_digital("C_AckAlarm",                    ARCHIVE=" 1Hz",                              PV_DESC="CMD: Acknowledge Alarm")
add_digital("C_Auto",                        ARCHIVE=" 1Hz",                              PV_DESC="CMD: Auto Mode")
add_digital("C_Manual",                      ARCHIVE=" 1Hz",                              PV_DESC="CMD: Manual Mode")
add_digital("C_Clear",                       ARCHIVE=" 1Hz",                              PV_DESC="CMD: clear console")

############################
#  PARAMETER BLOCK
############################
define_parameter_block()

#Limits
add_analog("DecrCompLimit-SP",   "REAL",     ARCHIVE=" 1Hz",     PV_PREC="2",      PV_DESC="Decreasion limit for compare")
add_analog("IncrCompLimit-SP",   "REAL",     ARCHIVE=" 1Hz",     PV_PREC="2",      PV_DESC="Increasion limit for compare")
add_analog("Deadband-SP",        "REAL",     ARCHIVE=" 1Hz",     PV_PREC="4",      PV_DESC="Deadband wide for stable status")
add_analog("SamplingTime-SP",    "REAL",     ARCHIVE=" 1Hz",     PV_PREC="2",      PV_DESC="Sampling time for readings in s")

#Communication for pyepics
add_string("Messages",39,           ARCHIVE=" 1Hz",      PV_NAME="Messages",      PV_DESC="PyEpics Messages from Arch")
add_string("EndDate-SP",10,           ARCHIVE=" 1Hz",      PV_NAME="EndDate-SP",      PV_DESC="End archiver of period")
add_string("StartDate-SP",10,           ARCHIVE=" 1Hz",      PV_NAME="StartDate-SP",      PV_DESC="Start archiver of period")
add_analog("UsageSum",    "REAL",     ARCHIVE=" 1Hz",     PV_PREC="2", PV_EGU="m3",      PV_DESC="Sum of usega from external script")


