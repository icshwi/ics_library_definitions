

################################# Version: 1.0 ##########################################
# ACCP_CRYO_GC_Type1
# Author:  Peyman Sheykholeslami
# Date:    20-03-2024
# Version: v1.0
# Note: Based on existing CosyLab IOC PV records
#########################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()




#Inhibit signals (set by the PLC code, can't be changed by the OPI)


#for OPI visualization

add_analog("Out",   	            "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",     PV_DESC="Out",                 PV_EGU="%")
add_analog("IntSp",   	        	"REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",     PV_DESC="IntSp",           	PV_EGU="%")
add_analog("ActSp",	        	    "REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",     PV_DESC="ActSp", 	            PV_EGU="%")
add_analog("ActVal",	        	"REAL",  ARCHIVE=" 1Hz",     PV_PREC="2",     PV_DESC="ActVal", 	            PV_EGU="%")

add_digital("CtlrOn",    	ARCHIVE=" 1Hz",    PV_DESC="CtlrOn",    PV_ONAM="True",             PV_ZNAM="False")

add_enum("StateVis","INT",   PV_NAME="StateVis",  PV_DESC="Status", SCAN="I/O Intr",  PV_NOBT="16", PV_ZRVL="0",      PV_ONVL="1",       PV_TWVL="2",     PV_THVL="3", PV_ZRST="Off",  PV_ONST="Manual On",  PV_TWST="Bypass On",  PV_THST="Control On", PV_ZRSV="MAJOR",  ARCHIVE=False )

#Transmitter value



#Alarm signals




#Feedback

add_analog("BypVal-RB",   	"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="BypVal-RB",      PV_EGU="%")
add_analog("OffVal-RB",   	"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="OffVal-RB",      PV_EGU="%")
add_analog("Sp-RB",   		"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="Sp-RB",   	     PV_EGU="%")
add_analog("RmpUp-RB",		"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="RmpUp-RB", 	     PV_EGU="%/s")
add_analog("RmpDn-RB", 		"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="RmpDn-RB",       PV_EGU="%/s")
add_analog("Gain-RB",  		"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="Gain-RB",        PV_EGU=" ")
add_analog("LowLim-RB",  	"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="LowLim-RB",      PV_EGU="%")
add_analog("HiLim-RB",   	"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="HiLim-RB",       PV_EGU="%")
add_analog("Tim-RB",   		"DWORD",       ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="Tim-RB",         PV_EGU="ms")
add_analog("DeadB-RB",  	"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="DeadB-RB",       PV_EGU="%")
add_analog("Sp1-RB",    	"REAL",        ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="Sp1-RB",         PV_EGU="%")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

add_analog("BypVal",   	"REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="BypVal",       PV_EGU="%")
add_analog("OffVal",   	"REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="OffVal",       PV_EGU="%")
add_analog("Sp",   		"REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="Sp",   	     PV_EGU="%")
add_analog("RmpUp",		"REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="RmpUp", 	     PV_EGU="%/s")
add_analog("RmpDn", 	"REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="RmpDn",  	     PV_EGU="%/s")
add_analog("Gain",  	"REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="Gain",   	     PV_EGU=" ")
add_analog("LowLim",  	"REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="LowLim", 	     PV_EGU="%")
add_analog("HiLim",   	"REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="HiLim",        PV_EGU="%")
add_analog("Tim",   	"DINT",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="Time",         PV_EGU="ms")
add_analog("DeadB",     "REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="DeadB",        PV_EGU="%")
add_analog("Sp1",       "REAL",  ARCHIVE=" 1Hz",  PV_PREC="2",   PV_DESC="Sp1",          PV_EGU="%")

#Forcing

